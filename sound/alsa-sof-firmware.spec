# This is a firmware package, so binaries (which are not run on the host)
# in the end package are expected.
%define _binaries_in_noarch_packages_terminate_build   0
%global _firmwarepath  /lib/firmware
%global _xz_opts -9 --check=crc32

%global tplg_version 1.2.8

Summary:        Firmware and topology files for Sound Open Firmware project
Name:           alsa-sof-firmware
Version:        2.2.5
Release:        1
# See later in the spec for a breakdown of licensing
License:        BSD
Group:          Sound
URL:            https://github.com/thesofproject/sof-bin
Source0:        https://github.com/thesofproject/sof-bin/releases/download/v%{version}/sof-bin-v%{version}.tar.gz
# Additional firmware from Aquarius and other OEM partners
# Also present here:
# https://git.altlinux.org/gears/f/firmware-alsa-sof.git?p=firmware-alsa-sof.git;a=commitdiff;h=19c20a01927cb28b4773c61125aff27bd99aad0a
# https://git.altlinux.org/gears/f/firmware-alsa-sof.git?p=firmware-alsa-sof.git;a=commitdiff;h=3e1039d55a64f83cb43fdacbef2b9f4c6857557e
Source11:       sof-cml-es8336.tplg
Source14:       sof-tgl-es8326.tplg
BuildRequires:  alsa-utils >= %{tplg_version}
BuildRequires:  alsa-topology >= %{tplg_version}

# noarch, since the package is firmware
BuildArch:      noarch

%description
This package contains the firmware binaries for the Sound Open Firmware project.

%package debug
Requires:       alsa-sof-firmware
Summary:        Debug files for Sound Open Firmware project
License:        BSD

%description debug
This package contains the debug files for the Sound Open Firmware project.


%prep
%autosetup -n sof-bin-v%{version}

mkdir -p firmware/intel/sof

# we have the version in the package name
mv sof-v%{version}/* firmware/intel/sof

# move topology files
mv sof-tplg-v%{version} firmware/intel/sof-tplg

for i in %{SOURCE11} %{SOURCE14}
do
  filename="$(basename "$i")"
  if test -f firmware/intel/sof-tplg/"$filename"; then
    echo "$filename already exists in upstream!"
    exit 1
  fi
  cp "$i" firmware/intel/sof-tplg/
done

# remove NXP firmware files
rm LICENCE.NXP
rm -rf firmware/intel/sof-tplg/sof-imx8*

# remove Mediatek firmware files
rm -rf firmware/intel/sof-tplg/sof-mt8*

# use xz compression
find -P firmware/intel/sof -type f -name "*.ri" -exec xz -z %{_xz_opts} {} \;
for f in $(find -P firmware/intel/sof -type l -name "*.ri"); do \
  l=$(readlink "${f}"); \
  d=$(dirname "${f}"); \
  b=$(basename "${f}"); \
  rm "${f}"; \
  pushd "${d}"; \
  ln -svf "${l}.xz" "${b}.xz"; \
  popd; \
done
find -P firmware/intel/sof-tplg  -type f -name "*.tplg" -exec xz -z %{_xz_opts} {} \;

%build
# SST topology files (not SOF related, but it's a Intel hw support
# and this package seems a good place to distribute them
alsatplg -c /usr/share/alsa/topology/hda-dsp/skl_hda_dsp_generic-tplg.conf \
         -o firmware/skl_hda_dsp_generic-tplg.bin
# use xz compression
xz -z %{_xz_opts} firmware/*.bin
chmod 0644 firmware/*.bin.xz

%install
mkdir -p %{buildroot}%{_firmwarepath}
cp -ra firmware/* %{buildroot}%{_firmwarepath}

# gather files and directories
FILEDIR=$(pwd)
pushd %{buildroot}/%{_firmwarepath}
find -P . -name "*.ri.xz" | sed -e '/^.$/d' >> $FILEDIR/alsa-sof-firmware.files
#find -P . -name "*.tplg" | sed -e '/^.$/d' >> $FILEDIR/alsa-sof-firmware.files
find -P . -name "*.ldc" | sed -e '/^.$/d' > $FILEDIR/alsa-sof-firmware.debug-files
find -P . -type d | sed -e '/^.$/d' > $FILEDIR/alsa-sof-firmware.dirs
popd

sed -i -e 's:^./::' alsa-sof-firmware.{files,debug-files,dirs}
sed -i -e 's!^!%{_firmwarepath}/!' alsa-sof-firmware.{files,debug-files,dirs}
sed -e 's/^/%%dir /' alsa-sof-firmware.dirs >> alsa-sof-firmware.files

cat alsa-sof-firmware.files

%files -f alsa-sof-firmware.files
%license LICENCE*
%doc README*
%dir %{_firmwarepath}

# Licence: 3-clause BSD
%{_firmwarepath}/*.bin.xz

# Licence: 3-clause BSD
# .. for files with suffix .tplg
%{_firmwarepath}/intel/sof-tplg/*.tplg.xz

# Licence: SOF (3-clause BSD plus others)
# .. for files with suffix .ri
%files debug -f alsa-sof-firmware.debug-files
